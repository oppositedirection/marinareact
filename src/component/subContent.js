import React from "react";
export function SubContent() {
  return (
    <div className="container">
      <section className="pt-4 pb-6">
        <div className="pb-lg-4">
          <p className="subtitle text-secondary">One-of-a-kind directory app</p>
          <h2 className="mb-5">Discover great local businesses around you</h2>
        </div>
        <div className="row">
          <div className="col-sm-6 col-lg-3 mb-3 mb-lg-0">
            <div className="px-0 pr-lg-3">
              <div className="icon-rounded mb-3 bg-secondary-light"></div>
              <h3 className="h6 text-uppercase">Find the perfect place</h3>
              <p className="text-muted text-sm">
                One morning, when Gregor Samsa woke from troubled dreams, he
                found himself transformed in his bed in
              </p>
            </div>
          </div>
          <div className="col-sm-6 col-lg-3 mb-3 mb-lg-0">
            <div className="px-0 pr-lg-3">
              <div className="icon-rounded mb-3 bg-primary-light"></div>
              <h3 className="h6 text-uppercase">Book your seats</h3>
              <p className="text-muted text-sm">
                The bedding was hardly able to cover it and seemed ready to
                slide off any moment. His many legs, pit
              </p>
            </div>
          </div>
          <div className="col-sm-6 col-lg-3 mb-3 mb-lg-0">
            <div className="px-0 pr-lg-3">
              <div className="icon-rounded mb-3 bg-secondary-light"></div>
              <h3 className="h6 text-uppercase">Enjoy your evening</h3>
              <p className="text-muted text-sm">
                His room, a proper human room although a little too small, lay
                peacefully between its four familiar{" "}
              </p>
            </div>
          </div>
          <div className="col-sm-6 col-lg-3 mb-3 mb-lg-0">
            <div className="px-0 pr-lg-3">
              <div className="icon-rounded mb-3 bg-primary-light"></div>
              <h3 className="h6 text-uppercase">Earn points</h3>
              <p className="text-muted text-sm">
                Samsa was a travelling salesman - and above it there hung a
                picture that he had recently cut out of{" "}
              </p>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
