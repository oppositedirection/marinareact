import React, { useState, useEffect } from "react";
import Geocode from "react-geocode";
import Card from "./card";
import {Loader} from './loader'

export function Marinas(props) {
  // set variable for query input
  const query = props.query;

  // convert address to coordinates using Geocode
  const [coordinates, setCoordinates] = useState();
  useEffect(() => {
    if (query.length > 3) {
      Geocode.setApiKey("AIzaSyD-gudwLvYl5vDncg3THPkqVtky7lwfVl4");
      Geocode.enableDebug();
      Geocode.fromAddress(`${query}`).then(
        response => {
          setCoordinates(response.results[0].geometry.location);
        },
        error => {
          console.error(error);
        }
      );
    }
  }, [query]); // React Hook useEffect has an unnecessary dependency: 'query'. Either exclude it or remove the dependency array. Outer scope values like 'query' aren't valid dependencies because mutating them doesn't re-render the component.eslint(react-hooks/exhaustive-deps)

  // API key from marinas.com
  const marinaApi = "oVfW7ZJxY-VU1ZjP4kXx";

  // Fetching list of marinas

  const [marinas, setMarinas] = useState({});
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    if (coordinates) {
      async function getMarinas(a, b, c) {
        let response = await fetch(
          `https://api.marinas.com/v1/marinas/search?location[lat]=${a}&location[lon]=${b}&access_token=${c}&radius=1`
        );
        let data = await response.json();
        setLoading(false)
        return data;
      }

      getMarinas(coordinates.lat, coordinates.lng, marinaApi).then(data =>
        setMarinas(data.data)
      );
    }
   

  }, [coordinates]); 

  

  if (loading) {
    return (
      <Loader/>
    );
  }

  return (
    <React.Fragment>
    {Object.keys(marinas).map(key => (
      <Card
        key={marinas[key].id}
        name={marinas[key].name}
        images={marinas[key].images.data}
      />
    ))}
  </React.Fragment>
   
  );
}
