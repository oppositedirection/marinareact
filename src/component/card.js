import React from "react";
import { Link } from "react-router-dom"

function Card(props) {
  return (
    <div className="container">
      <div className="row">
        <div className="d-flex align-items-lg-stretch mb-4 col-lg-8">
          {props.images.map((image, index) => (
            <div
              key={index}
              style={{
                background: "center center",
                backgroundImage: `url(${image.full_url})`,
                backgroundSize: "cover"
              }}
              className="card shadow-lg border-0 w-100 border-0"
            >
              <div className="d-flex align-items-center h-100 text-white justify-content-center py-6 py-lg-7">
                <h3 className="text-shadow text-uppercase mb-0">
                  <Link style={{ color: "white"}} to="/">{props.name}</Link>
                </h3>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Card;
