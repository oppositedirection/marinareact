import React from "react";
import { Link } from 'react-router-dom'

export function SearchBar(props) {
    const value = props.query
    const func = props.handleChange

  return (
  
  <section>
      <div className="container">
        <div className="search-bar rounded p-3 p-lg-4 position-relative mt-n5 z-index-20">
          <form action="#">
            <div className="row">
              <div className="col-lg-9 d-flex align-items-center form-group">
                <input value={value} onChange={func} type="search" name="search" placeholder="What are you searching for?" className="form-control border-0 shadow-0 font-two-em" />
              </div>

              <div className="col-lg-3 form-group mb-0">
                <Link to='/s/results'>
                  <button className="btn btn-primary btn-block">Search</button>
                </Link>
              </div>
            </div>
          </form>
        </div>
      </div>

  
    </section>
  )
}
  