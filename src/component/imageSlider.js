import React from "react";
export function ImageSlider() {
  return <section id="dark" style={{
    backgroundImage: "url(https://image.shutterstock.com/z/stock-photo-sail-boat-gliding-in-open-sea-at-sunset-209357422.jpg)"
  }} className="d-flex align-items-center dark bg-cover">
        <div className="container py-6 py-lg-7 text-white overlay-content text-center">
          <div className="row">
            <div className="col-xl-10 mx-auto">
              <h1 className="display-3 font-weight-bold text-shadow">
                Your one-stop guide to maritime fun and adventure!
              </h1>
            </div>
          </div>
        </div>
      </section>;
}
  