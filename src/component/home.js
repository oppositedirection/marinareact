import React from 'react';
import { Navbar } from "./navbar";
import { ImageSlider } from "./imageSlider";
import { SearchBar } from "./searchBar";
import { FeatureMarinas } from "./featureMarinas";
import { SubContent } from "./subContent"
 
 export function Home(props){
   
  return (
    <React.Fragment>
      <Navbar />
      <ImageSlider />
      <SearchBar
          query={props.query}
          handleChange={props.handleChange}
          handleSubmit={props.handleSubmit}
      />
      <FeatureMarinas />
      <SubContent />

    </React.Fragment>
  )
}



