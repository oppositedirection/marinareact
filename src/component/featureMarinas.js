import React from "react";
export function FeatureMarinas() {
  return (
    <section>
      <div className="container">
        <div className="row ">
          <div className="col">
            <h2 className="text-white"> Features Marinas</h2>
          </div>

          <div className="col text-right text-white">See All Features</div>
        </div>

        <div className="row">
          <div className="d-flex align-items-lg-stretch mb-4 col-lg-8">
            <div
              style={{
                background: "center center",
                backgroundImage:
                  " url(https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1449034446853-66c86144b0ad.jpg)",
                backgroundSize: "cover"
              }}
              className="card shadow-lg border-0 w-100 border-0"
            >
              <div className="d-flex align-items-center h-100 text-white justify-content-center py-6 py-lg-7">
                <h3 className="text-shadow text-uppercase mb-0">
                  San Francisco
                </h3>
              </div>
            </div>
          </div>
          <div className="d-flex align-items-lg-stretch mb-4 col-lg-4">
            <div
              style={{
                background: "center center",
                backgroundImage:
                  " url(https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1429554429301-1c7d5ae2d42e.jpg)",
                backgroundSize: "cover"
              }}
              className="card shadow-lg border-0 w-100 border-0"
            >
              <div className="d-flex align-items-center h-100 text-white justify-content-center py-6 py-lg-7">
                <h3 className="text-shadow text-uppercase mb-0">Los Angeles</h3>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="d-flex align-items-lg-stretch mb-4 col-lg-4">
            <div
              style={{
                background: "center center",
                backgroundImage:
                  " url(https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1523430410476-0185cb1f6ff9.jpg)",
                backgroundSize: "cover"
              }}
              className="card shadow-lg border-0 w-100 border-0"
            >
              <div className="d-flex align-items-center h-100 text-white justify-content-center py-6 py-lg-7">
                <h3 className="text-shadow text-uppercase mb-0">
                  Santa Monica
                </h3>
              </div>
            </div>
          </div>
          <div className="d-flex align-items-lg-stretch mb-4 col-lg-4">
            <div
              style={{
                background: "center center",
                backgroundImage:
                  " url(https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1505245208761-ba872912fac0.jpg)",
                backgroundSize: "cover"
              }}
              className="card shadow-lg border-0 w-100 border-0"
            >
              <div className="d-flex align-items-center h-100 text-white justify-content-center py-6 py-lg-7">
                <h3 className="text-shadow text-uppercase mb-0">San Diego</h3>
              </div>
            </div>
          </div>
          <div className="d-flex align-items-lg-stretch mb-4 col-lg-4">
            <div
              style={{
                background: "center center",
                backgroundImage:
                  " url(https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1519867850-74775a87e783.jpg) ",
                backgroundSize: "cover"
              }}
              className="card shadow-lg border-0 w-100 border-0"
            >
              <div className="d-flex align-items-center h-100 text-white justify-content-center py-6 py-lg-7">
                <h3 className="text-shadow text-uppercase mb-0">Fresno</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
