import React, { useState } from "react";
import { Marinas } from "./component/marinas";
import { Home } from "./component/home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  const [query, setQuery] = useState("");

  function handleChange(e) {
    setQuery(e.target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();
  }

  return (
    <React.Fragment>
      <Router>
        <Switch>
          <Route
            exact
            path="/"
            render={props => (
              <Home
                {...props}
                query={query}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
              />
            )}
          />

          <Route
            exact
            path="/s/results"
            render={props => <Marinas {...props} query={query} />}
          />
        </Switch>
      </Router>
    </React.Fragment>
  );
}

export default App;
